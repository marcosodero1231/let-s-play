//******************************Preloader********************

window.onload = function () {setTimeout(function openWindow () {

    document.getElementById('window-loader').classList.toggle('window-loader-off-opacity');
  
  },3300);

  {setTimeout(function openWindow () {

    document.getElementById('window-loader').classList.toggle('window-loader-off');
  
  },4400);};

};

function preloadAnimLogo () {
    setTimeout (function(){
        document.getElementById('preloader-animation').classList.add('open-animation-one')
    }, 1500)

    setTimeout (function(){
        document.getElementById('preloader-animation').classList.add('open-animation-two')
    },2500)
}

preloadAnimLogo();


//******************************nav-menu-buttons********************

function clickUserSection () {
  document.getElementById('users-button').classList.remove('focus-class-nav');
  document.getElementById('home-button').classList.remove('focus-class-nav');
  document.getElementById('user-button').classList.add('focus-class-nav');
  document.getElementById('my-user-section').classList.add('visible-section');
  document.getElementById('my-user-section').classList.remove('section-off');
  document.getElementById('users-section').classList.add('section-off');
  document.getElementById('home-section').classList.add('section-off');
}

document.getElementById('user-button').addEventListener("click", clickUserSection);

function clickHomeSection () {
  document.getElementById('users-button').classList.remove('focus-class-nav');
  document.getElementById('home-button').classList.add('focus-class-nav');
  document.getElementById('user-button').classList.remove('focus-class-nav');
  document.getElementById('my-user-section').classList.add('section-off');
  document.getElementById('users-section').classList.add('section-off');
  document.getElementById('home-section').classList.remove('section-off');
  document.getElementById('home-section').classList.add('visible-section-flex');
}

document.getElementById('home-button').addEventListener("click", clickHomeSection);


function clickUsersSection () {
  document.getElementById('users-button').classList.add('focus-class-nav');
  document.getElementById('home-button').classList.remove('focus-class-nav');
  document.getElementById('user-button').classList.remove('focus-class-nav');
  document.getElementById('my-user-section').classList.add('section-off');
  document.getElementById('users-section').classList.remove('section-off');
  document.getElementById('users-section').classList.add('visible-section');
  document.getElementById('home-section').classList.add('section-off');
}

document.getElementById('users-button').addEventListener("click", clickUsersSection);

//***************************Arr-Map-Filter */

/* const users = [
  {name: "Tomás", game: "Basquet", city: "Cordoba", age: 21},
  {name: "Carlos", game: "Futbol", city: "Tucumán", age: 15 },
  {name: "Juan", game: "Pádel", city: "Corrientes", age: 29 },
  {name: "Jorge", game: "Handball", city: "San luis", age: 42 },
  {name: "Manuel", game: "Tenis", city: "Jujuy", age: 14 },
]

function activeWindowArr () {
  var printHtml = users.map(
    element => document.getElementById('my-user-section').innerHTML += ` <div style="text-align: left;"> Bienvenido 
    ${element.name} que juega al ${element.game} en ${element.city}
    <br> <br> </div>`
  )

  var filterUsers = users.filter(
    
    element => element.age <= 18
  )
  console.log(filterUsers);
}

document.getElementById('user-button').addEventListener("click", activeWindowArr); */



//***************************API-Clase-3*/

function initialFunctApi () {
  document.getElementById('users-section').classList.remove('window-loader-off');
  const resolveOrReject = new Promise((resolve, reject) => {
    setTimeout(() => resolve(), 0000)
  })
  

  resolveOrReject.then(() =>{
    loadUsersApi();
  })
}

 function loadUsersApi () {
   document.getElementById('child-container-dad').innerHTML = '';
   document.getElementById('load-animation-id').classList.remove('window-loader-off')
   document.getElementById('users-section').classList.add('overflow-loading-hidden')
   fetch("https://jsonplaceholder.typicode.com/users")
  .then((response) => response.json())
  .then((json) => printUsers(json));

 }


 function printUsers(usersObject) {
   const container =  document.getElementById('child-container-dad');
   usersObject.forEach(usersObject => {     
     const containerChild = document.createElement('div')            
     containerChild.innerHTML = `
        <div>
        <img url="">
        </div>
      <div class="user-card">
        <h1 class="name-users">${usersObject.name}</h1>
        <h3 class="city-user">Está buscando jugadores en ${usersObject.address.city}</h3>
      </div>
    `;
    container.appendChild(containerChild);
    setTimeout(() => {
      document.getElementById('load-animation-id').classList.add('window-loader-off');
      document.getElementById('users-section').classList.remove('overflow-loading-hidden')
    }, 4000);
   });
 }

document.getElementById('users-button').addEventListener("click",initialFunctApi); 